@extends('layouts.mvp')

@section('content')
  <!-- Top Toolbar -->
  
  
  <!-- HOME PRO-->
  <div class="home-pro"> 
    
    <!-- PRO BANNER HEAD -->
    <div class="banr-head">
      <div class="container">
        <div class="row"> 
          
          <!-- CONTENT -->
          <div class="col-sm-7">
            <div class="text-area">
              <div class="position-center-center col-md-10">
                <h1> Here comes the social networking platform that you’ve been waiting for</h1>
                <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur. </h6>
              </div>
            </div>
          </div>
          
          <!-- FORM SECTION -->
          <div class="col-sm-5">
            <div class="login-sec"> 
              
              <!-- TABS -->
              <div class="uou-tabs">
                <ul class="tabs">
                  <li><a href="#register">Register Now</a></li>
                  <li class="active"><a href="#log-in">Member Login</a></li>
                </ul>
                
                <!-- REGISTER -->
                <div class="content">
                  <div id="register">
                         <div style='width:200px;margin:0 auto'>
                      <label>Student</label>
                      <input type="radio" value='1' name='regType' id="regTypeSt" checked>
                       <label>Professor</label>
                      <input type="radio" value='2' name='regType' id="regTypeProf">
                      </div>
                      <div id="regStud">
                    <form action='{{route('student.register.submit')}}' method='post' name='registerFrm' id='registerFrm'>
                      {{ csrf_field() }}
                     
                      <div style='width:150px;margin:0 auto'>
                      <label>Male</label>
                      <input type="radio" value='1' name='sex' checked>
                       <label>Female</label>
                      <input type="radio" value='2' name='sex'>
                      </div>
                      <input type="text" name='name' placeholder="Name">
                      <span id="error_name"></span>
                      <input type="text" name='surename' placeholder="Surename">
                       <span id="error_surename"></span>
                      <input name='city' type="text" placeholder="City">
                      <span id="error_city"></span>
                      <input name='email' type="email" placeholder="Email Address">
                      <span id="error_email"></span>
                      {!! Form::selectMonth('month')!!}
                     {!!  Form::selectRange('day', 1, 31)!!}
                      {!!  Form::selectRange('year', date('Y'), 1930)!!}
                     <label class="custom-file-upload">
                            <input type="file" name='image'/>
                            Upload image
                        </label>
                      <span id="error_image"></span>
                      <input name='password' type="password" placeholder="Password">
                       <input name='password_confirmation' type="password" placeholder="Password confirmation">
                     
                       <span id="error_password"></span><br>
                      <button type="button" id='regBtn'>Register</button>
                      <div class="login-with"> <span>Or login with:</span> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-google"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> </div>
                      
                    </form>
                      </div>
                        <div id="regProf" style="display:none">
                         
                    <form action='{{route('professor.register.submit')}}' method='post' name='registerProfFrm' id='registerProfFrm'>
                      {{ csrf_field() }}
                      <div style='width:150px;margin:0 auto'>
                      <label>Male</label>
                      <input type="radio" value='1' name='sex' checked>
                       <label>Female</label>
                      <input type="radio" value='2' name='sex'>
                      </div>
                      <input type="text" name='name' placeholder="Name">
                      <span id="error_nameProf"></span>
                      <input type="text" name='surename' placeholder="Surename">
                       <span id="error_surenameProf"></span>
                      <input name='city' type="text" placeholder="City">
                      <span id="error_cityProf"></span>
                      <input name='email' type="email" placeholder="Email Address">
                      <span id="error_emailProf"></span>
                      {!! Form::selectMonth('month')!!}
                     {!!  Form::selectRange('day', 1, 31)!!}
                      {!!  Form::selectRange('year', date('Y'), 1930)!!}
                     <label class="custom-file-upload">
                            <input type="file" name='image'/>
                            Upload image
                        </label>
                      <span id="error_imageProf"></span>
                      <input name='password' type="password" placeholder="Password">
                       <input name='password_confirmation' type="password" placeholder="Password confirmation">
                     
                       <span id="error_passwordProf"></span>
                       <textarea placeholder="Description" name="description" style="height:100px"></textarea>
                       <span id="error_descriptionProf"></span>
                       <br>
                      <button type="button" id='regProfBtn'>Register</button>
                      <div class="login-with"> <span>Or login with:</span> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-google"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> </div>
                      
                    </form>
                      </div>
                  </div>
                  
                  <!-- LOGIN -->
                  <div id="log-in" class="active">
                   <div style='width:200px;margin:0 auto'>
                      <label>Student</label>
                      <input type="radio" value='1' name='logType' id="logTypeSt" checked>
                       <label>Professor</label>
                      <input type="radio" value='2' name='logType' id="logTypeProf">
                      </div>
                    <form action="{{route('student.login.submit')}}" method="post" name="loginFrm" id="loginFrm">
                        {{ csrf_field() }}
                      <input type="text" name="email" placeholder="Email Address" value="{{ old('email') }}">
                      
                     
                      
                      
                      @if($errors->has('email'))
                        <span class="error_span">
                          {{ $errors->first('email') }}
                        </span>
                      @endif
                     
                      <input type="password" name="password" placeholder="Password">
                       @if($errors->has('password'))
                        <span class="error_span">
                          {{ $errors->first('password') }}
                        </span>
                      @endif
                          @if($errors->has('field'))
                        <span class="error_span">
                          {{ $errors->first('field') }}
                        </span>
                      @endif
                      <br>
                      <button type="submit">Login</button>
                      <div class="login-with"> <span>Or login with:</span> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-google"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> </div>
                      <div class="forget">Forgot your password? <a href="#.">Click Here</a></div>
                    </form>
                  </div>
                  <div id="forget">
                    <form>
                      <input type="email" placeholder="Email Address">
                      <button type="submit">Login</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- SERVICES -->
    <section class="services"> 
      
      <!-- SERVICES ROW -->
      <ul class="row">
        
        <!-- SECTION -->
        <li class="col-md-4">
          <div class="ser-inn">
          <i class="fa fa-globe"></i>
            <h4>Stay in touch with your
              colleagues</h4>
            <i class="fa fa-globe big"></i>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>
          </div>
        </li>
        
        <!-- SECTION -->
        <li class="col-md-4">
          <div class="ser-inn">
            <i class="fa fa-book"></i>
            <h4>Get the latest news
              in your industry</h4>
            <i class="fa fa-book big"></i>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>
          </div>
        </li>
        
        <!-- SECTION  -->
        <li class="col-md-4">
          <div class="ser-inn">
          <i class="fa fa-picture-o"></i>
            <h4>Share what’s up
              with you</h4>
            <i class="fa fa-picture-o big"></i>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>
          </div>
        </li>
      </ul>
    </section>
    
    <!-- PRO SECTION -->
    <section class="pro-content">
      <div class="container-fluid">
        <div class="row"> 
          
          <!-- PRO IMAGE -->
          <div class="col-md-6 pro-inside" style="background:url(images/pro-img-1.jpg) center center no-repeat;"></div>
          
          <!-- PRO CONTENT -->
          <div class="col-md-6 pro-inside">
            <div class="position-center-center col-md-6">
              <h1>Interact with other
                professionals</h1>
              <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, 
                eaque ipsa quae ab illo inventore veritatis et quasi architecto 
                beatae vitae dicta sunt explicabo. </p>
            </div>
          </div>
        </div>
      </div>
      
      <!-- PRO SECTION -->
      <div class="container-fluid">
        <div class="row"> 
          
          <!-- PRO TEXT -->
          <div class="col-md-6 pro-inside">
            <div class="position-center-center col-md-6">
              <h1>Collaborate on a
                project</h1>
              <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, 
                eaque ipsa quae ab illo inventore veritatis et quasi architecto 
                beatae vitae dicta sunt explicabo. </p>
            </div>
          </div>
          
          <!-- PRO BACKGROUND -->
          <div class="col-md-6 pro-inside" style="background:url(images/pro-img-2.jpg) center center no-repeat;"></div>
        </div>
      </div>
    </section>
    
    <!-- APP IMAGE -->
    <section class="app-images">
      <div class="container">
        <div class="row"> 
          
          <!-- TEXT -->
          <div class="col-md-6 text-center text-area">
            <h1>SocialMe for your 
              Smartphone</h1>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
              accusantium doloremque laudantium, totam rem aperiam, 
              eaque ipsa quae ab illo inventore veritatis et quasi architecto 
              beatae vitae dicta sunt explicabo. </p>
            <a href="#."><i class="fa fa-apple"></i> App Store</a> </div>
          
          <!-- APP IMAGE -->
          <div class="col-md-6 text-right"><img src="images/app-img.png" alt="" > </div>
        </div>
      </div>
    </section>
    
    <!-- TESTIMONIALS -->
    <section class="clients-says">
      <div class="container">
        <h3 class="section-title">what our users say </h3>
        <div class="testi">
          <div class="texti-slide"> 
            <!-- SLide -->
            <div class="clints-text">
              <div class="text-in">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>
              </div>
              <div class="avatar">
                <div class="media-left"> <a href="#."> <img src="images/clients-avatar-1.jpg" alt=""> </a> </div>
                <div class="media-body">
                  <h6>John Kevin Mara</h6>
                  <span>smashingmagazine.com</span> </div>
              </div>
            </div>
            
            <!-- SLide -->
            <div class="clints-text">
              <div class="text-in">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>
              </div>
              <div class="avatar">
                <div class="media-left"> <a href="#."> <img src="images/clients-avatar-1.jpg" alt=""> </a> </div>
                <div class="media-body">
                  <h6>John Kevin Mara</h6>
                  <span>smashingmagazine.com</span> </div>
              </div>
            </div>
            
            <!-- SLide -->
            <div class="clints-text">
              <div class="text-in">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>
              </div>
              <div class="avatar">
                <div class="media-left"> <a href="#."> <img src="images/clients-avatar-1.jpg" alt=""> </a> </div>
                <div class="media-body">
                  <h6>John Kevin Mara</h6>
                  <span>smashingmagazine.com</span> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- sponsors -->
    <div class="sponsors" style="background:url(images/sponser-bg.jpg) no-repeat;">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="section-title">Our Sponsors</h3>
            <div class="sponsors-slider">
              <div class="item"><img src="images/sponsor_logo1.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo2.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo3.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo4.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo5.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo6.png" alt="" class="img-responsive"></div>
              <div class="item"><img src="images/sponsor_logo4.png" alt="" class="img-responsive"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
 
    
    $("#regBtn").click(function(){
          // var data = $("#registerFrm").serialize();
           var data = $("#registerFrm").serialize();
           var formData = new FormData($("#registerFrm")[0]);
           var regArray=["name","surename","city","email","image","password"];
         $.ajax({
                url: '{{route("student.register.submit")}}',
                type: 'POST',
                dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false, 
                contentType: false,
                success: function(data, textStatus, jqXHR) {
                    //console.log(data+" sve ok")
                    
                    if(data.success==false)
                    {
                        for (j in regArray)
                        {
                            $("#error_"+regArray[j]).html("");
                            $("#error_"+regArray[j]).removeClass("error_span");
                        }
                        var keys=Object.keys(data.error);
                        for (i in keys)
                        {
                          //console.log(data.error[keys[i]]);
                       // $("#error_"+keys[i]).html("");
                          $("#error_"+keys[i]).addClass("error_span");
                          //console.log(data.error[keys[i]]);
                          for (k in data.error[keys[i]])
                          {
                          $("#error_"+keys[i]).append(data.error[keys[i]][k]+"<br>");
                          }
                        }    
                    }
                    else
                    {
                      //  window.location="/afterRegistration";
                      for (j in regArray)
                        {
                            $("#error_"+regArray[j]).html("");
                            $("#error_"+regArray[j]).removeClass("error_span");
                        }
                        alertify.alert("You have registered successfully!");
                    }
                    /*if (data.error == false) {
                        $('#name_lastname').val('');
                        $('#mail').val('');
                        //$('#status').html('<p>'+data.message+'</p>').fadeIn(500).delay(5000).fadeOut(500);
                        window.location = "http://kapijavracara.rs/newsletter-success";
                    }

                    if(data.error == true){
                        $('#status').html('<p>'+data.message+'</p>').css('color', 'red').fadeIn(500).delay(5000).fadeOut(500);
                    }*/
                },

                 error: function(data){
                    var data = data.responseJSON;
                    console.log(data + "error");
                   /* $.each( data, function( key, value ) {
                        $('input[name="'+key+'"]').removeClass('valid');
                        $('input[name="'+key+'"]').addClass('error');
                    });*/
                },

            });
    });
    $("#regProfBtn").click(function(){
          // var data = $("#registerFrm").serialize();
           var data = $("#registerFrm").serialize();
           var formData = new FormData($("#registerProfFrm")[0]);
           var regArray=["name","surename","city","email","image","password","description"];
         $.ajax({
                url: '{{route("professor.register.submit")}}',
                type: 'POST',
                dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                processData: false, 
                contentType: false,
                success: function(data, textStatus, jqXHR) {
                    //console.log(data+" sve ok")
                    
                    if(data.success==false)
                    {
                        for (j in regArray)
                        {
                            $("#error_"+regArray[j]+"Prof").html("");
                            $("#error_"+regArray[j]+"Prof").removeClass("error_span");
                        }
                        var keys=Object.keys(data.error);
                        for (i in keys)
                        {
                          //console.log(data.error[keys[i]]);
                       // $("#error_"+keys[i]).html("");
                          $("#error_"+keys[i]+"Prof").addClass("error_span");
                          //console.log(data.error[keys[i]]);
                          for (k in data.error[keys[i]])
                          {
                          $("#error_"+keys[i]+"Prof").append(data.error[keys[i]][k]+"<br>");
                          }
                        }    
                    }
                    else
                    {
                      //  window.location="/afterRegistration";
                      for (j in regArray)
                        {
                            $("#error_"+regArray[j]+"Prof").html("");
                            $("#error_"+regArray[j]+"Prof").removeClass("error_span");
                        }
                        alertify.alert("You have registered successfully!");
                    }
                    /*if (data.error == false) {
                        $('#name_lastname').val('');
                        $('#mail').val('');
                        //$('#status').html('<p>'+data.message+'</p>').fadeIn(500).delay(5000).fadeOut(500);
                        window.location = "http://kapijavracara.rs/newsletter-success";
                    }

                    if(data.error == true){
                        $('#status').html('<p>'+data.message+'</p>').css('color', 'red').fadeIn(500).delay(5000).fadeOut(500);
                    }*/
                },

                 error: function(data){
                    var data = data.responseJSON;
                    console.log(data + "error");
                   /* $.each( data, function( key, value ) {
                        $('input[name="'+key+'"]').removeClass('valid');
                        $('input[name="'+key+'"]').addClass('error');
                    });*/
                },

            });
    });
    
     $("#regTypeSt").click(function(){
        $("#regStud").css("display","block");
         $("#regProf").css("display","none");
     })
      $("#regTypeProf").click(function(){
        $("#regStud").css("display","none");
         $("#regProf").css("display","block");
     })
     
        $("#logTypeSt").click(function(){
            $("#loginFrm").attr('action', '{{route('student.login.submit')}}');
     })
      $("#logTypeProf").click(function(){
        $("#loginFrm").attr('action', '{{route('professor.login.submit')}}');
     })

            </script>
            @endsection
