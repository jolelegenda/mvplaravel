<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'Mvp\HomeController@index')->name('default');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('student')->group(function() {
  //  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/register', 'Auth\StudentRegisterController@create')->name('student.register.submit');
    Route::post('/login', 'Auth\StudentLoginController@login')->name('student.login.submit');
    Route::get('/logout', 'Auth\StudentLoginController@logout')->name('student.logout');
 //   Route::get('/', 'AdminController@index')->name('admin.dashboard');
});
Route::prefix('professor')->group(function() {
  //  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/register', 'Auth\ProfessorRegisterController@create')->name('professor.register.submit');
    Route::post('/login', 'Auth\ProfessorLoginController@login')->name('professor.login.submit');
    Route::get('/logout', 'Auth\ProfessorLoginController@logout')->name('professor.logout');
 //   Route::get('/', 'AdminController@index')->name('admin.dashboard');
});
