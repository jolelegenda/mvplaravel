<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('professors', function (Blueprint $table) {
             $table->increments('id');
            $table->string('name',100);
            $table->string('surname',100);
            $table->string('city',100);
            $table->string('email',100)->unique();
            $table->dateTime('dateofbirth')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->text('description');
            $table->tinyInteger('sex');
            $table->tinyInteger('status')->default(0);
            
            $table->string('password',250);
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professors');
    }
}
