<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSchedulersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedulers', function (Blueprint $table) {
             $table->increments('id');
            $table->string('subject',150);
            $table->tinyInteger('start');
            $table->tinyInteger('classTypeId');
            $table->tinyInteger('classContentId');
            $table->integer('studentId')->unsigned();;
            $table->text('description');
            $table->dateTime('currentDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_schedulers');
    }
}
