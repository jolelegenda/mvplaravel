<?php

namespace Modules\Professor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\ClassScheduler;
use App\ClassSchedule;
use App\ProfessorsVideo;
use Illuminate\Support\Facades\Validator;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:professor');
        //$this->middleware('auth:professor')->only("index");
    }
    
    public function index()
    {
        
       $professor=\Auth::guard('professor')->user();
       $classschedulers=ClassScheduler::orderBy("id","desc")->get();
       
        return view('professor::index',["professor"=>$professor,"classschedulers"=>$classschedulers]);
    }
  
     public function getSchedule($id)
    {
          $professor=\Auth::guard('professor')->user();
        $classscheduler=ClassScheduler::find($id);
        
        return view('professor::ajxClass',['classscheduler'=> $classscheduler,"professor"=>$professor]);
        
    }
  
    public function applyToClass($idSchedule,$idProfessor)
    {
        $schedule=new ClassSchedule([
            'scheduleId' => $idSchedule,
            'professorId' => $idProfessor
          
        ]);
        
          $saved=$schedule->save();
          if($saved)
          echo "You applied successfully";
          else
              echo "there was some error";
    }
    
    public function uploadVideo(Request $request)
    {
        $professor=\Auth::guard('professor')->user();
         $credentials = $request->only('introVideo');
 //$credentials = $request->only('name','surename', 'city', 'email', 'password','password_confirmation');
        $rules = [
           'introVideo' => 'required|mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi|max:2048',
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
           // dd( $validator->errors());
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
       // var_dump(Request::hasFile('introVideo'));
         if($request->hasFile('introVideo')){

            $file = $request->file('introVideo');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/uploads/';
           $file->move($path, $filename);
        }
        //var_dump($_FILES);
         $professor=ProfessorsVideo::create([
            'name' => $filename ,
            'professorId' => $professor->id,
           
        ]);
     
            
            return response()->json(['success'=> true, 'error'=> '']);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('professor::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('professor::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('professor::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
