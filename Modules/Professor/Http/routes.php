<?php

Route::group(['middleware' => 'web', 'prefix' => 'professor', 'namespace' => 'Modules\Professor\Http\Controllers'], function()
{
     Route::get('/profile', 'ProfessorController@index')->name("professor.profile");
     Route::get('/listing', 'ProfessorPublicController@listing')->name("professor.listing");
     Route::get('/show/{id}', 'ProfessorPublicController@show')->name("professor.show");
     Route::get('/class/{id}', 'ProfessorController@getSchedule')->name("professor.schedule");
     Route::get('/applyClass/{idSchedule}/{idProfessor}', 'ProfessorController@applyToClass')->name("professor.apply");
     Route::post('/uploadVideo', 'ProfessorController@uploadVideo')->name("professor.introVideo");
});
