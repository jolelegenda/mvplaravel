@extends('layouts.mvp')

@section('content')


   <section class="pro-mem">
    <div class="container pb30">
      <h3>Professors</h3>
      <div class="row">
          @if(!$professors->isEmpty())
          @foreach($professors as $professor) 
        <div class="col-sm-3">
          <div class="uou-block-6a"> <img src="{{asset('images/upload/'.$professor->images[0]->name)}}" alt="">
              <h6><a href="{{route("professor.show",["id"=>$professor->id])}}">{{$professor->name." ".$professor->surname}}</a><span>{{ str_limit($professor->description, 30) }}</span></h6>
            <p><i class="fa fa-map-marker"></i>{{$professor->city}}</p>
          </div>
          <!-- end .uou-block-6a --> 
        </div>
          @endforeach
          @endif
      </div>
    </div>
  </section>

<!-- end .uou-block-4e -->
@endsection
