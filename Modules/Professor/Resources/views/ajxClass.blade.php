<div class="napuni">
<div style="padding:10px;">
    <h2>{{$classscheduler->subject}}</h2>
    <strong>Request date</strong>: {{date('d-m-Y', strtotime($classscheduler->currentDate))}}<br><br>
    <strong>Start time</strong>: {{date('d-m-Y H:i', strtotime($classscheduler->start))}}<br><br>
    <strong>Class type</strong>: {{$classscheduler->classType->name}}<br><br>
    <strong>Class content</strong>: {{$classscheduler->classContent->name}}<br><br>
    <strong>Description</strong>: {{$classscheduler->description}}<br><br>
      @php
        $app = false;
        if($classscheduler->professorId>0)
        {
           $app=true;
        }
      @endphp  
    @foreach($classscheduler->schedules()->orderBy('professors.id')->get() as $sched) 
       @if($sched->id==$professor->id)
       You have applied for this class<br>
            @php
              $app = true
            @endphp
        @endif
    @endforeach
        @if(!$app)
        <button type="button" id="applyClass" class="btn btn-primary">Apply</button>
        @endif
        @if($classscheduler->professorId==$professor->id)
         You have been chosen for this class by {{$classscheduler->student->name}} {{$classscheduler->student->surname}} ({{$classscheduler->student->email}})<br>
        @endif
       
        
</div>
<script>
    $("#applyClass").click(function(){
        
        
alertify.confirm("Would you like to apply for this class?",
  function(){
       $.ajax({
                url: '{{route("professor.apply",["idSchedule"=>$classscheduler->id,"idProfessor"=>$professor->id])}}',
                type: 'GET',
                //dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                //data:{},
              
                success: function(data, textStatus, jqXHR) {
                    //console.log(data+" sve ok")
                    
                  //alert(data);
                  //$("#applyClass").hide();
                  alertify.alert(data);
                  ajaxCall('','{{route("professor.schedule",["id"=>$classscheduler->id])}}',function(data){
                      $(".napuni").html(data);
                  },'GET')
                 
                },

                 error: function(data){
                    var data = data.responseJSON;
                    console.log(data + "erro44r");
                   /* $.each( data, function( key, value ) {
                        $('input[name="'+key+'"]').removeClass('valid');
                        $('input[name="'+key+'"]').addClass('error');
                    });*/
                },

            });
  },
  function(){
   // alertify.error('Cancel');
  });
       
    })
  
</script>
</div>
