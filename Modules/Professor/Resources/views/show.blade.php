@extends('layouts.mvp')

@section('content')

  <div class="compny-profile"> 
    <!-- SUB Banner -->

    
    <!-- Profile Company Content -->
    <div class="profile-company-content" data-bg-color="f5f5f5" style="margin-top:2px;">
      <div class="container">
        <div class="row"> 
          
          <!-- SIDE BAR -->
          <div class="col-md-4"> 
            
            <!-- Company Information -->
            <div class="sidebar">
              <h5 class="main-title">Professor Information</h5>
              <div class="sidebar-thumbnail"> <img src="{{asset('images/upload/'.$professor->images[0]->name)}}" alt=""> </div>
              <div class="sidebar-information">
                <ul class="single-category">
                  <li class="row">
                    <h6 class="title col-xs-6">Full name</h6>
                    <span class="subtitle col-xs-6">{{$professor->name." ".$professor->surname}}</span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Location</h6>
                    <span class="subtitle col-xs-6">{{$professor->city}}</span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Date of brth</h6>
                    <span class="subtitle col-xs-6">{{date('d-m-Y', strtotime($professor->dateofbirth))}}</span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Gender</h6>
                    <span class="subtitle col-xs-6">
                        
                        @if($professor->sex==1)
                        Male
                        @endif 
                        @if($professor->sex==2)
                        Female
                        @endif
                    </span> </li>
                  
                </ul>
              </div>
            </div>
            
            <!-- Company Rating -->
            <div class="sidebar">
              <h5 class="main-title">Professor Rating</h5>
              <div class="sidebar-information">
                <ul class="single-category com-rate">
                  <li class="row">
                    <h6 class="title col-xs-6">Expertise:</h6>
                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Knowledge:</h6>
                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i></span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Quality::</h6>
                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Price:</h6>
                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span> </li>
                  <li class="row">
                    <h6 class="title col-xs-6">Services:</h6>
                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></span> </li>
                </ul>
              </div>
            </div>
            
            <!-- Company Rating -->
            <div class="sidebar">
              <h5 class="main-title">Contact</h5>
              <div class="sidebar-information form-side">
                <form action="#">
                  <input type="text" placeholder="Name & Surname">
                  <input type="text" placeholder="E-mail address">
                  <textarea placeholder="Your Message"></textarea>
                  <button class="btn btn-primary">Send message</button>
                </form>
              </div>
            </div>
          </div>
          
          <!-- Tab Content -->
          <div class="col-md-8">
            <div class="tab-content"> 
              
              <!-- PROFILE -->
              <div id="profile" class="tab-pane fade in active">
                <div class="profile-main">
                  <h3>Description</h3>
                  <div class="profile-in">
                   
                    <p>{{$professor->description}} </p>
                    
                  </div>
                </div>
                
                <!-- Services -->
                <div class="profile-main">
                  <h3>Services</h3>
                  <div class="profile-in profile-serv">
                    <h6>Here’s an overview of the services we provide.</h6>
                    <div class="media">
                      <div class="media-left">
                        <div class="icon"> <img src="images/icon-prifile-1.png" alt="" > </div>
                      </div>
                      <div class="media-body">
                        <h6>Engine diagnostics and repairs</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, 
                          mollitia, voluptatibus similique aliquidautem laudantium sapiente ad enim ipsa modi 
                          labo rum accusantium deleniti neque.</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="media-left">
                        <div class="icon"> <img src="images/icon-prifile-2.png" alt="" > </div>
                      </div>
                      <div class="media-body">
                        <h6>Engine diagnostics and repairs</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, 
                          mollitia, voluptatibus similique aliquidautem laudantium sapiente ad enim ipsa modi 
                          labo rum accusantium deleniti neque.</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="media-left">
                        <div class="icon"> <img src="images/icon-prifile-3.png" alt="" > </div>
                      </div>
                      <div class="media-body">
                        <h6>Engine diagnostics and repairs</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, 
                          mollitia, voluptatibus similique aliquidautem laudantium sapiente ad enim ipsa modi 
                          labo rum accusantium deleniti neque.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- end .uou-block-4e -->
@endsection