<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Student\Entities\ClassType;
use Modules\Student\Entities\ClassContent;
use Illuminate\Support\Facades\Validator;
use App\ClassScheduler;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:student');
    }
    
    public function index()
    {
        // $idStudent=\Auth::guard('student')->user()->id;
         $student=\Auth::guard('student')->user();
       //$student= Student::with('classschedulers')->get()->find(21);
        // dd($student);
         $classtypes=ClassType::all();
         $classcontents=ClassContent::all();
   

        return view('student::index',['student'=>$student,'classtypes'=>$classtypes,'classcontents'=>$classcontents]);
    }
    public function loadClassesAjax()
    {
      $student=\Auth::guard('student')->user();
      return view('student::ajxClasses',['student'=>$student]);
    }
    public function scheduleClass(Request $request)
    {
             $credentials = $request->only('subject','start', 'classType', 'classContent','studentId','description');
 //$credentials = $request->only('name','surename', 'city', 'email', 'password','password_confirmation');
            $rules = [
                'subject' => 'required|string|min:2|max:150',
                'start' => 'required|date_format:"d-m-Y H:i:s"',
                'classType' => 'required|numeric|min:1',
                'classContent' => 'required|numeric|min:1',
                'studentId' => 'required|numeric|min:1',
               'description' => 'required|string|min:30'
            ];

            $validator = Validator::make($credentials, $rules);

            if($validator->fails()) {
               // dd( $validator->errors());
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
            $timestamp = strtotime($request->input('start'));
            $datestart=date('Y-m-d H:i:s',  $timestamp);
            
             $scheduler=ClassScheduler::create([
            'subject' => $request->input('subject'),
            'start' => $datestart,
            'classTypeId' => $request->input('classType'),
            'classContentId' => $request->input('classContent'),
            'studentId' =>$request->input('studentId'),
            'description' => $request->input('description')
        ]);
             return response()->json(['success'=> true, 'error'=> ""]);
    }
    public function getSchedule($id)
    {
        $classscheduler=ClassScheduler::find($id);
       // debug($classscheduler);
       
        return view('student::ajxClass',['classscheduler'=> $classscheduler]);
        
    }
    
    public function chooseProf($idProfessor,$idScheduler)
    {
        $classscheduler=ClassScheduler::find($idScheduler);
        if($classscheduler->professorId==0)
        {
            $classscheduler->professorId=$idProfessor;
            $saved=$classscheduler->save();
            if($saved)
            {
                echo "You have chosen professor successfully";
            }
            else 
            {
                echo "Something went wrong";
            }
        }
        else
        {
            echo "Professor for this class is already chosen";
        }
        
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('student::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('student::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('student::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
