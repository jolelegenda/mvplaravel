<?php

Route::group(['middleware' => 'web', 'prefix' => 'student', 'namespace' => 'Modules\Student\Http\Controllers'], function()
{
    Route::get('/profile', 'StudentController@index')->name("student.profile");
    Route::post('/shedule', 'StudentController@scheduleClass')->name('student.schedule.submit');
    Route::get('/class/{id}', 'StudentController@getSchedule')->name("student.schedule");
    Route::get('/chooseProf/{idProfessor}/{idScheduler}', 'StudentController@chooseProf')->name("choose.professor");
    Route::get('/loadClasses', 'StudentController@loadClassesAjax')->name("student.classes");
});
