<div class="napuni">
<div style="padding:10px;">
    <h2>{{$classscheduler->subject}}</h2>
    Request date: {{date('d-m-Y', strtotime($classscheduler->currentDate))}}<br><br>
    Start time: {{date('d-m-Y H:i', strtotime($classscheduler->start))}}<br><br>
    Class type: {{$classscheduler->classType->name}}<br><br>
    Class content: {{$classscheduler->classContent->name}}<br><br>
    Description: {{$classscheduler->description}}<br><br>
   
    Professors applied:<br>
    @php
    $professor_set=0;
 
     if($classscheduler->professorId>0)
     {
             $professor_set=1;
            
     }
        @endphp
    @foreach($classscheduler->schedules()->orderBy('professors.id')->get() as $sched) 
  - <a href="{{route("professor.show",["id"=>$sched->id])}}" target="__blank">{{$sched->name}} {{$sched->surname}}</a>   <?=($professor_set<1)?'- <a title="Choose this professor" data-profname="'.$sched->name." ".$sched->surname.'" data-profroute="'.route("choose.professor",["idProfessor"=>$sched->id,"idScheduler"=>$classscheduler->id]).'" href="#" class="chooseProf"><i class="fa fa-pencil" aria-hidden="true"></i></a>':""?><?=($sched->id==$classscheduler->professorId)?"<i class=\"fas fa-check-double\"></i>":""?><br>
    @endforeach
</div>
<script>
    $(".chooseProf").click(function(e){
        e.preventDefault();
    var prof_name=$(this).attr("data-profname");
       var prof_rou=$(this).attr("data-profroute");
      
alertify.confirm("Would you like to choose this professor ("+prof_name+")?",
  function(){
       $.ajax({
                url: prof_rou,
                type: 'GET',
                //dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                //data:{},
              
                success: function(data, textStatus, jqXHR) {
                    //console.log(data+" sve ok")
                    
                  //alert(data);
                  //$(".chooseProf").hide();
                  alertify.alert(data);
                  ajaxCall('','{{route("student.schedule",["id"=>$classscheduler->id])}}',function(data){
                      $(".napuni").html(data);
                  },'GET')
                 
                },

                 error: function(data){
                    var data = data.responseJSON;
                    console.log(data + "erro44r");
                   /* $.each( data, function( key, value ) {
                        $('input[name="'+key+'"]').removeClass('valid');
                        $('input[name="'+key+'"]').addClass('error');
                    });*/
                },

            });
  },
  function(){
   // alertify.error('Cancel');
  });
       
    })
  
</script>
</div>
