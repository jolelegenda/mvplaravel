  <ul>
                        @foreach($student->classschedulers()->orderBy('id','desc')->get() as $classscheduler)
                        <!-- MEMBER -->
                        <li>
                          <div class="row">

                              <div class="col-xs-3 n-p-r n-p-l"> <span>Subject:<br> {{$classscheduler->subject}}</span> </div>
                            <!-- Location -->
                            <div class="col-xs-3 n-p-r n-p-l"> <span>Start time<br> {{$classscheduler->start->format("d-m-Y H:i")}}</span> </div>
                            <!-- Network -->
                            <div class="col-xs-3 n-p-r"> <span>Class type:<br> {{$classscheduler->classType->name}}</span> </div>
                            <!-- Connections -->
                             <!--<div class="col-xs-3 n-p-r"> <span>Content:<br> {{$classscheduler->classContent->name}}</span> </div>-->
                            <div class="col-xs-2 n-p-r n-p-l"> <span>Content:<br> {{$classscheduler->classContent->name}}</span> </div>
                               
                           <div class="col-xs-1 n-p-r n-p-l"> <span> <a class="ajax" href="{{route("student.schedule",["id"=>$classscheduler->id])}}" title="{{$classscheduler->subject}}">See details</a></span> </div>
                          </div>
                             
                        </li>
                       @endforeach
                      {{-- $student->classschedulers()->paginate(4) {!!$student->classschedulers()->paginate(4)->links("vendor.pagination.simple-bootstrap-4")!!}--}}
                            
                      </ul>
<script> 
    $(".ajax").colorbox({});
</script>