<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassScheduler extends Model
{
    protected $fillable = [
        'subject', 'start','classTypeId','classContentId','studentId','description'
    ];
    protected $dates=["start"];
    
    public function classContent()
    {
        return $this->belongsTo('Modules\Student\Entities\ClassContent', 'classContentId', 'id');
    }
    public function classType()
    {
        return $this->belongsTo('Modules\Student\Entities\ClassType', 'classTypeId', 'id');
    }
    public function schedules()
    {
        return $this->belongsToMany('App\Professor', 'class_schedules', 'scheduleId', 'professorId');
    }
    public function student()
    {
        return $this->belongsTo('App\Student', 'studentId', 'id');
    }
}
