<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessorsVideo extends Model
{
    protected $fillable = [
        'professorId', 'name'
    ];
}
