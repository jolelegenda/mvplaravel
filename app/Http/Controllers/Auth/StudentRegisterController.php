<?php

namespace App\Http\Controllers\Auth;

use App\Student;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class StudentRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|min:5|max:100',
            'surname' => 'required|string|min:5|max:100',
            'city' => 'required|string|min:5|max:100',
            'email' => 'required|string|email|max:255|unique:students',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
     //  return $request->all();
      $credentials = $request->only('name','surename', 'city', 'email','image', 'password','password_confirmation');
 //$credentials = $request->only('name','surename', 'city', 'email', 'password','password_confirmation');
        $rules = [
            'name' => 'required|string|min:2|max:100',
            'surename' => 'required|string|min:2|max:100',
            'city' => 'required|string|min:2|max:100',
            'email' => 'required|string|email|max:255|unique:students',
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|min:6|max:100|confirmed'
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
           // dd( $validator->errors());
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
      //  return "123";die;
        /*$image = $request->file('image');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/images/upload');

        $image->move($destinationPath, $input['imagename']);
*/
          $image =  $request->file('image');
          $filename  = time() . '.' . $image->getClientOriginalExtension();

           $path = public_path('images/upload/' . $filename);
 
        
           \Image::make($image->getRealPath())->resize(200, 200)->save($path);
                /*$user->image = $filename;
                $user->save();*/

       
       $student=Student::create([
            'name' => $request->input('name'),
            'surname' => $request->input('surename'),
            'city' => $request->input('city'),
            'email' => $request->input('email'),
            'dateofbirth' => $request->input('year')."-".$request->input('month')."-".$request->input('day')." "."00:00:00",
            'sex' => $request->input('sex'),
            'status' => 0,
            'password' => bcrypt($request->input('password'))
        ]);
      // print_r($student->id);
            $student_id=$student->id;
      
            $image=new \App\Image();
            $image->name=$filename;
            $image->is_profile=1;
            $image->student_id=$student_id;
            $image->save();
            
             return response()->json(['success'=> true, 'error'=> '']);
    }
}
