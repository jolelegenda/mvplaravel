<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use Notifiable;
    
    protected $guard = 'student';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname','city','email','dateofbirth','sex','status', 'password'
    ];
   /* protected $fillable = [
        'name','email','password'
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function images()
    {
        return $this->hasMany('App\Image', 'student_id', 'id');
    }
    public function classschedulers()
    {
        return $this->hasMany('App\ClassScheduler', 'studentId', 'id');
    }
    
}

