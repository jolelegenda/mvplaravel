<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Professor extends Authenticatable
{
        use Notifiable;
    
    protected $guard = 'professor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname','city','email','dateofbirth','description','sex','status', 'password'
    ];
   /* protected $fillable = [
        'name','email','password'
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function scopeStatus($query, $param)
    {
        return $query->where('status', $param);
    }
    
    public function images()
    {
        return $this->hasMany('App\Image', 'professor_id', 'id');
    }
    public function intro_video()
    {
        return $this->hasOne('App\ProfessorsVideo', 'professorId', 'id'); 
    }
    
}
